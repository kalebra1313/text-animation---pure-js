const editor = new FroalaEditor('#editor', {
    toolbarButtons: ['bold', 'italic', 'underline', 'fontSize', 'alignLeft', 'alignCenter', 'alignRight', 'undo', 'redo'],
    pluginsEnabled: ['align', 'fontSize'],
    fontFamily: {
        'Helvetica,sans-serif': 'Font 1'
    },
    fontFamilyDefaultSelection: 'Font 1',
    pastePlain: true
});
const animator = document.getElementById('text');

const loopThrough = (element, underlined = false) => {
    let ind = 0;
    for (let child of element.childNodes) {
        if (child.data) {
            const mainS = document.createElement('span');

            let lined = '';
            if (underlined) lined = 'lined';

            let result = '';
            for (let i = 0; i < child.data.length; i++) {
                result += `<span class="toAnimate ${lined}">${child.data[i]}</span>`;
            }
            mainS.innerHTML = result;
            element.removeChild(element.childNodes[ind]);
            element.insertBefore(mainS, element.childNodes[ind]);
        } else {
            if (child.tagName === 'U') {
                let span = document.createElement('span');
                span.innerHTML = child.innerHTML;
                element.removeChild(element.childNodes[ind]);
                element.insertBefore(span, element.childNodes[ind]);
                loopThrough(span, true)
            } else {
                loopThrough(child)
            }
        }

        ind++;
    }
};

const animate = () => {
    const elements = document.getElementsByClassName('toAnimate');

    let i = 1;
    for (let element of elements) {
        setTimeout(() => {
            element.style.color = 'inherit';
        }, i * 35);

        i++;
    }
};

const showAnimation = () => {
    let text = editor.html.get();

    if (text) {
        animator.innerHTML = text;
        for (let child of animator.childNodes) {
            if (child.childNodes.length) loopThrough(child);
        }

        animate();
    }
};
